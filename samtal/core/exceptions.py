from samtal.core.team_members import TeamMember


class UnreachableTeamMemberError(Exception):

    def __init__(self, team_member: TeamMember, inner: Exception) -> None:
        super().__init__(f"Unreachable TeamMember {team_member}", inner)
        self.member = team_member
