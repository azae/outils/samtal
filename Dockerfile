FROM python:3.6
WORKDIR /usr/src/app
RUN pip install pipenv
COPY Pipfile Pipfile.lock ./
RUN pipenv sync
RUN pipenv run python -c "import nltk; nltk.download('punkt')"
COPY . .
CMD ["pipenv", "run", "python", "-m", "samtal"]
