#!/bin/bash
set -x
pipenv install --dev
pipenv run python -c "import nltk; nltk.download('punkt')"
