#!/bin/bash

rm -rf build
rm -rf dist
sed -i -e "s/VERSION/$1/g" setup.py
pipenv run ./setup.py bdist_wheel --universal
pipenv run ./setup.py sdist
pipenv run twine upload dist/* -u azae -p $PYPI_PASSWORD
